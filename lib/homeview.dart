import 'package:flutter/material.dart';
import 'package:kemenkes/splashscreen.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:connectivity/connectivity.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late bool isLoading;

  @override
  void initState() {
    checkConnection();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (isLoading == true)
          ? WebView(
              initialUrl: 'https://dki-keluargasehat.kemkes.go.id/login',
              javascriptMode: JavascriptMode.unrestricted,
            )
          : FailedConnection(),
    );
  }

  checkConnection() async {
    var connection = await Connectivity().checkConnectivity();
    if (connection == ConnectivityResult.none) {
      setState(() {
        isLoading = false;
      });
      return FailedConnection;
    } else {
      setState(() {
        isLoading = true;
      });
    }
  }
}

class FailedConnection extends StatelessWidget {
  const FailedConnection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Tidak Ada Koneksi Internet"),
      content: Text("Silahkan periksa koneksi internet anda dan coba lagi"),
      actions: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.white)),
            onPressed: () => Navigator.push(
                context, MaterialPageRoute(builder: (_) => SplashScreen())),
            child: Text(
              "Tutup",
              style: TextStyle(color: Colors.pink),
            )),
      ],
    );
  }
}
